import React, { Component } from 'react';
import request from 'request';
import { Link } from 'react-router-dom'
import LoadingIcon from './../../components/LoadingIcon';

import {result} from 'lodash'

class CharacterDetails extends Component {

  componentDidMount(){
    this.fetchDetails()
  }

  fetchDetails(){

    const id = this.props.match.params.id

    request({
      method: 'GET',
      uri: `https://swapi.co/api/people/${id}/`
    }, (err, response, body) => {

      if(err || response.statusCode !== 200) {
        window.location = `${window.location.origin}/404`
        // console.error('unable to fetch character details from API')
      }else{
        body = JSON.parse(body)

        this.setState({
          charDetails: body
        })
      }

    })
  }

  render() {

    if(!result(this, 'state.charDetails')) return <LoadingIcon />

    return (
      <div className="card mt-5 col-md-4 ml-auto mr-auto">
        <div className="card-body">
          <h4 className="card-title">{this.state.charDetails.name}</h4>
        </div>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">Height: {this.state.charDetails.height}</li>
          <li className="list-group-item">Mass: {this.state.charDetails.mass}</li>
          <li className="list-group-item">Hair Color: {this.state.charDetails.hair_color}</li>
          <li className="list-group-item">Skin Color: {this.state.charDetails.skin_color}</li>
          <li className="list-group-item">Birth Year: {this.state.charDetails.birth_year}</li>
          <li className="list-group-item">Gender: {this.state.charDetails.gender}</li>
        </ul>
        <div className="card-body">
          <Link to="/" className="btn btn-primary">Go Back</Link>
        </div>
      </div>
    )
  }
}

export default CharacterDetails;
