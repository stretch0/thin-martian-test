import React, { Component } from 'react';
// import './Contact.css';

class NotFound extends Component {
 render() {
   return (
     <div className="notfound">
       <p>This is not the page you're looking for</p>
       <img src="https://flowtationdevices.files.wordpress.com/2011/05/kenobi_011.jpg" alt="obi-wan"/>
     </div>
   );
 }
}
export default NotFound;