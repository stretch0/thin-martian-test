import React, { Component } from 'react';
import CharcterList from './../../components/CharacterList'

class Home extends Component {
 render() {
   return (
	  <div className="home container mb-5">
	    <CharcterList />
     </div>
   );
 }
}

export default Home;