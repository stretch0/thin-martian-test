import React, { Component } from 'react';

class LoadingIcon extends Component {
	render(){
		return(
			<div >
				<img className="loading-icon mt-5 mb-5" src="/loading.gif" alt="loading" />
			</div>
		)
	}
}

export default LoadingIcon;