import React, { Component } from 'react';
import request from 'request';
import { Link } from 'react-router-dom';
import LoadingIcon from './LoadingIcon';
import star from './../star.svg'
import favStar from './../favStar.svg'
import { connect } from 'react-redux';
import * as characterActions from '../actions/characters';

class CharacterList extends Component {

  constructor(props){
    super(props)

    // temp var to store list of chars when fetching from API
    this.charList = []
  }

  componentDidMount(){
    this.fetchCharList()
  }

  fetchCharList(uri) {

    // first request uses base uri and then when we fetch other pages we use next URI
    if(!uri) uri = 'https://swapi.co/api/people';

    // make API call
    request({
      method: 'GET',
      uri: uri
    }, (err, response, body) => {
      
      // if failed response
      if(err || response.statusCode !== 200) {
        console.error('unable to fetch people from API')
      }else{ // success

        body = JSON.parse(body)

        // get list of already fetched characters
        const charList = this.charList

        // push the new results into the existing array
        charList.push(...body.results)

        // if there is another page of results, fetch them as well, 
        // otherwise we set state of loading to false
        if(body.next) {
          this.fetchCharList(body.next)
        }else{
          // only update if characters array is empty
          if(this.props.characters.length < 1) this.props.dispatch( characterActions.populate(charList) )
        }

      }

    })
  }

  sortByFirstName(){

    const charList = this.props.characters.sort((a, b) => {
      if(a.name < b.name) return -1;
      if(a.name > b.name) return 1;
      return 0;
    }) 

    this.setState({ charList })
  }

  addToFav(charName){

    if(!this.props.favourites.includes(charName)){
      this.props.dispatch( characterActions.addToFav(charName) )
    } else{
      this.props.dispatch( characterActions.removeFromFav(charName) )
    }

    

  }

  render() {

    if(this.props.characters.length < 1) return <LoadingIcon />
    
    return (
      <div className="row">
        <div className="col-md-8 m-auto">
          
          <div className="clearfix mb-4 mt-4">
            <button className="float-right btn btn-primary" onClick={() => {this.sortByFirstName() }}>Sort By First Name</button>
          </div>

          <div className="list-group">
            
            <div className="list-group-item" >
              <h5>Star Wars Characters</h5>
            </div>

            {this.props.characters.map((char, key) => {

              // splits the url into an array so we can extract the ID
              const parts = char.url.split('/')

              return (
                <div key={key} className="list-group-item list-group-item-action flex-column align-items-start" >
                  <div className="d-flex w-100 justify-content-between">
                    <h5 className="mb-1">{char.name}</h5>
                    <Link to={`/character/${parts[5]}`}>View Profile</Link>
                  </div>
                  <div className="d-flex w-100 justify-content-end">
                    <img onClick={() => {this.addToFav(char.name) }} src={this.props.favourites.includes(char.name) ? favStar : star} className="star" alt="logo" />
                  </div>
                </div>
              )

            })}
          </div>

        </div>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    favourites: state.favourites,
    characters: state.characters
  };
}

export default connect(mapStateToProps)(CharacterList);
