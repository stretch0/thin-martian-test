export const populate = (characters) => {
  return {
    type: 'populate',
    characters
  };
}

export const addToFav = (character) => {
  return {
    type: 'addFav',
    character
  };
}

export const removeFromFav = (character) => {
  return {
    type: 'removeFav',
    character
  };
}

