import characters from './characters';
import favourites from './favourites';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    characters,
    favourites
});

export default rootReducer;