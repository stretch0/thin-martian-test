export default(state = [], payload) => {
  switch (payload.type) {
    case 'addFav':
      return [...state, payload.character];
    case 'removeFav':
    	// this is bad practice as is actually mutating the state
    	return state.filter(element => element !== payload.character);
    default:
      return state;
  }
};