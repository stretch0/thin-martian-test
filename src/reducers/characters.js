export default(state = [], payload) => {
  switch (payload.type) {
    case 'populate':
    // console.log('...state', ...state)
      return [...state, ...payload.characters];
    default:
      return state;
  }
};