import React, { Component } from 'react';
import './App.css';
import { Switch, Route, Link } from 'react-router-dom'

import Home from './pages/Home/Home'
import CharacterDetails from './pages/CharacterDetails/CharacterDetails'
import NotFound from './pages/NotFound/NotFound'

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <Link to="/">
            <img src={'http://pngimg.com/uploads/star_wars_logo/star_wars_logo_PNG20.png'} className="App-logo" alt="logo" />
          </Link>
        </div>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route exact path='/character/:id' component={CharacterDetails}/>
          <Route path='*' component={NotFound}/>
        </Switch>
      </div>
    );
  }
}

export default App;
