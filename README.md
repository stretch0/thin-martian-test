This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

This is utilising create-react-app redux and react-router

The main react code is within the two components `CharacterList.js` and `CharacterDetails.js`.

### In order to get running, please run the following commands

	1. yarn install
	2. yarn start
	3. go to http://localhost:3000
